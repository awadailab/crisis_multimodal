# Multimodal Deep Learning For Disaster Response

## Dataset
The multimodal dataset (image and text for each post) is collected from social media sites (Twitter and Instagram) and labeled by a group of 5 volunteers. The dataset was used in the aforementioned paper and is available for download only for academic purposes using the following link:
https://drive.google.com/open?id=1JDnwlwfZHAuqccSqDjjSKXC4buJPW56B 

## Training 
More information about training can be found here: https://github.com/husseinmozannar/multimodal-deep-learning-for-disaster-response 

## Citation
Hussein Mouzannar, Yara Rizk, & Mariette Awad. (2018). Damage Identification in Social Media Posts using Multimodal Deep Learning. In Kees Boersma, & Brian Tomaszeski (Eds.), ISCRAM 2018 Conference Proceedings – 15th International Conference on Information Systems for Crisis Response and Management (pp. 529–543). Rochester, NY (USA): Rochester Institute of Technology.

